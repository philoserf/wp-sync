---
ID: 473
post_title: The End Was Built into the Beginning
author: Mark Ayers
post_excerpt: ""
layout: post
permalink: 'http://markpaulayers.com?p=473&preview=true&preview_id=473'
published: false
---
Our founding fathers, the history of Rome, and the end of the American republic.