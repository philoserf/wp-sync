---
ID: 277
post_title: Kit
author: Mark Ayers
post_excerpt: ""
layout: page
permalink: http://markpaulayers.com/kit/
published: true
post_date: 2016-07-04 01:00:00
---
Computing

- MacBook Pro, iPad pro, & iPhone X
- iTerm2, zsh, & antigen
- Atom &
- Python & Golang
- Wordpress & Hugo
- Things 3, & Scapple
- Pandora, Safari Queue, Audible & Kindle

Writing

- Rhodia paper
- Pilot Namiki fountain pen
- Pilot iroshizuku ama-iro ink

Driving

- Fiat 500 Sport

Running

- New Balance minimus
- Runkeeper

Backpacking

- Granite Gear Crown VC 60
- New Balance minimus trail