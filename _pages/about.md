---
ID: 44
post_title: Mark Ayers
author: Mark Ayers
post_excerpt: ""
layout: page
permalink: http://markpaulayers.com/
published: true
post_date: 2016-07-04 00:00:52
---
Mark is a human being. He lives in Edmonds, a small town in Washington State – United States of America. His home provides the advantages of a great city and a small coastal town in a region nestled between mountain ranges, divided by the Salish Sea, and a series of freshwater lakes and rivers. Mark lived in Italy for a year in the early 1980s and in Turkey for a year in the early 1990s. He earned a degree in political science at The University at Albany, State University of New York. Today he works on cloud-native computing technologies in Seattle USA.

_The views expressed here are his own. They are not endorsed, approved, or reviewed by any other person or organization._

# Contact Information

```plain
MARK AYERS
712 ALDER ST
EDMONDS WA 98020-3416
UNITED STATES OF AMERICA

mark@philoserf.com
+ 1.206.280.4061
```

**github: [@philoserf][github] | email: [mark@philoserf.com][email]**

[email]: mailto:mark@philoserf.com
[github]: http://github.com/philoserf