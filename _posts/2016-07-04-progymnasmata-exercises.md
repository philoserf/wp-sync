---
ID: 271
post_title: Progymnasmata Exercises
author: Mark Ayers
post_excerpt: >
  A set of rudimentary exercises intended
  to prepare students of rhetoric for the
  creation and performance of complete
  practice orations (gymnasmata or
  declamations). A crucial component of
  classical and renaissance rhetorical
  pedagogy. Many progymnasmata exercises
  correlate directly with the parts of a
  classical oration.
layout: post
permalink: >
  http://markpaulayers.com/progymnasmata-exercises/
published: true
post_date: 2016-07-04 08:35:14
---
A set of rudimentary exercises intended to prepare students of rhetoric for the creation and performance of complete practice orations (gymnasmata or declamations). A crucial component of classical and renaissance rhetorical pedagogy. Many progymnasmata exercises correlate directly with the parts of a classical oration.

1. [Fable][fable]
2. [Narrative][narration]
3. [Chreia][chreia]
4. [Proverb][proverb]
5. [Refutation][refutation]
6. [Confirmation][confirmation]
7. [Commonplace][commonplace]
8. [Encomium][encomium]
9. [Vituperation][vituperation]
10. [Comparison][comparison]
11. [Impersonation][impersonation]
12. [Description][description]
13. [Thesis or Theme][thesis]
14. [Defend / Attack a Law][law]

[http://rhetoric.byu.edu/Pedagogy/Progymnasmata/Progymnasmata.htm][source]

[chreia]: http://rhetoric.byu.edu/Pedagogy/Progymnasmata/Chreia.htm
[commonplace]: http://rhetoric.byu.edu/Pedagogy/Progymnasmata/Commonplace.htm
[comparison]: http://rhetoric.byu.edu/Pedagogy/Progymnasmata/Comparison.htm
[confirmation]: http://rhetoric.byu.edu/Pedagogy/Progymnasmata/Confirmation.htm
[description]: http://rhetoric.byu.edu/Pedagogy/Progymnasmata/Description.htm
[encomium]: http://rhetoric.byu.edu/Pedagogy/Progymnasmata/Encomium.htm
[fable]: http://rhetoric.byu.edu/Pedagogy/Progymnasmata/Fable.htm
[impersonation]: http://rhetoric.byu.edu/Pedagogy/Progymnasmata/Impersonation.htm
[law]: http://rhetoric.byu.edu/Pedagogy/Progymnasmata/Law.htm
[narration]: http://rhetoric.byu.edu/Pedagogy/Progymnasmata/Narration.htm
[proverb]: http://rhetoric.byu.edu/Pedagogy/Progymnasmata/Proverb.htm
[refutation]: http://rhetoric.byu.edu/Pedagogy/Progymnasmata/Refutation.htm
[source]: http://rhetoric.byu.edu/
[thesis]: http://rhetoric.byu.edu/Pedagogy/Progymnasmata/Thesis.htm
[vituperation]: http://rhetoric.byu.edu/Pedagogy/Progymnasmata/Vituperation.htm